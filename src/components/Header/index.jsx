import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import logo from "../../assets/img/LogoHz-green-spoon.png";
import "./styles.scss";
import "../../styles/responsive.scss";

import { scrollToTop } from '../../utils/scrollToTop';

function Header() {
  const [isActive, setIsActive] = useState(false);
  const [navScroll, setNavScroll] = useState("transparent");
  const listenScrollEvent = () => {
    if (window.pageYOffset > 0) {
      setNavScroll("rgb(34 141 90 / 92%)");
    } else {
      setNavScroll("transparent");
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", listenScrollEvent);
  }, [navScroll]);

  return (
    <section className="Header" style={{ backgroundColor: navScroll }}>
       <Link to="/">
      <div className="Header__logo" onClick={scrollToTop}>
        <img src={logo} alt="Logo The Green Spoon" />
      </div>
      </Link>
      <button
        className="Header__mobile"
        onClick={() => {
          setIsActive(!isActive);
        }}
      >
        <FontAwesomeIcon icon={faBars} />
      </button>

      {isActive ? (
        <div className="Header__links">
          <div className="Header__nav">
            <Link
              to="/"
              onClick={() => {
                setIsActive(false);
                scrollToTop();
              }}
            >
              <p>Inicio</p>
            </Link>
            <Link
              to="/blog"
              onClick={() => {
                setIsActive(false);
                scrollToTop();
              }}
            >
              <p>Blog</p>
            </Link>
            <Link
              to="/contact"
              onClick={() => {
                setIsActive(false);
                scrollToTop();
              }}
            >
              <p>Contacto</p>
            </Link>
          </div>

          <div className="Header__rrss">
            <a href="https://www.instagram.com/thegreen.spoon/">
              <FontAwesomeIcon
                icon={faInstagram}
                className="Header__rrss-icon"
              />
            </a>
          </div>
        </div>
      ) : null}

      <div className="Header__desktop">
        <div className="Header__desktop-nav">
          <Link
            to="/"
            onClick={() => {
              setIsActive(false);
              scrollToTop();
            }}
          >
            <p>Inicio</p>
          </Link>
          <Link
            to="/blog"
            onClick={() => {
              setIsActive(false);
              scrollToTop();
            }}
          >
            <p>Blog</p>
          </Link>
          <Link
            to="/contact"
            onClick={() => {
              setIsActive(false);
              scrollToTop();
            }}
          >
            <p>Contacto</p>
          </Link>
        </div>
        <div className="Header__desktop-line"></div>
        <div className="Header__desktop-rrss">
        <a href="https://www.instagram.com/thegreen.spoon/">
          <FontAwesomeIcon
            icon={faInstagram}
            className="Header__desktop-rrss-icon"
          />
         </a>
        </div>
      </div>
    </section>
  );
}

export default Header;
