import { Link } from "react-router-dom";

import { scrollToTop } from '../../utils/scrollToTop';

import "./styles.scss";

function HomeArticles({ artData }) {
  const newArrayArticles = artData.filter((article) => {
    return article.id > 6;
  });

  return (
    <section className="HomeArticles">
      {newArrayArticles.map((data, index) => {
        return (
          <article className="HomeArticles__article" key={data.id} id={data.id} onClick={scrollToTop}>
          <Link to={`/blog-post/${data.id}`}>
            <div className="HomeArticles__article-img">
              <img src={data.hero} alt={data.title} />
            </div>
            <h3 className="HomeArticles__title">{data.title}</h3>
            <p className="HomeArticles__copy">{data.copy}</p>

            <div className="HomeArticles__spans">
              <span>{data.date}</span>
              <div className="HomeArticles__line"></div>
              <span>{data.time}</span>
            </div>
            </Link>
          </article>
        );
      })}
    </section>
  );
}

export default HomeArticles;
