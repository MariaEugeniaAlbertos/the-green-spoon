import { Link } from "react-router-dom";

import { scrollToTop } from '../../utils/scrollToTop';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";

import "./styles.scss";
import logo from "../../assets/img/Logo-green-spoon-white.png";


function Footer() {
  return (
    <section className="Footer">
      <Link to="/">
        <div className="Footer__logo" onClick={scrollToTop}>
          <img src={logo} alt="The green spoon logo" />
        </div>
      </Link>
      <div className="Footer__legals">
        <p>@2021 The Green Spoon</p>
        <p>Avisos legales</p>
      </div>
      <div className="Footer__rrss">
        <a href="https://www.instagram.com/thegreen.spoon/">
          <FontAwesomeIcon icon={faInstagram} className="Footer__rrss-icon" />
        </a>
      </div>
    </section>
  );
}

export default Footer;
