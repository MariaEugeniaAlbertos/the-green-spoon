import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import emailjs, { send } from "emailjs-com";
import { init } from "emailjs-com";

import "./styles.scss";

init("user_4iTAuP681Q5WH2MJhQZyP");

function ContactForm() {
  const notify = () => {
    toast("Su mensaje se ha enviado correctamente :)", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  }

  function sendEmail(e) {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_ljvn5df",
        "template_vhnctzj",
        e.target,
        "user_4iTAuP681Q5WH2MJhQZyP"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    e.target.reset();
    
    notify()
    
  }  

  return (
    <form className="ContactForm" onSubmit={sendEmail}>
      <input type="hidden" name="contact_number" />

      <fieldset className="ContactForm__fieldset">
        <label>Nombre</label>
        <input
          type="text"
          name="user_name"
          className="ContactForm__input"
          required
        />
      </fieldset>
      <fieldset className="ContactForm__fieldset">
        <label>Email</label>
        <input
          type="email"
          name="user_email"
          className="ContactForm__input"
          required
        />
      </fieldset>
      <fieldset className="ContactForm__fieldset">
        <label>Mensaje</label>
        <textarea name="message" className="ContactForm__input" required />
      </fieldset>

      <button
        type="submit"
        value="Enviar"
        className="ContactForm__btn"
      >
        Enviar mensaje{" "}
      </button>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </form>
  );
}

export default ContactForm;
