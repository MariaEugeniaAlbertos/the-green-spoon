import './styles.scss';

import logoAdmin from '../../assets/img/about-icon-gestion.png';
import logoDesign from '../../assets/img/about-icon-diseño.png';
import logoFoot from '../../assets/img/about-icon-footprint.png';

function HomeAbout() {
    return (
        <section className="HomeAbout">
            <p className="HomeAbout__description">The Green Spoon nace con el objetivo de otorgar ese reconocimiento y valor a todos los proyectos sostenibles que se llevan a cabo en el mundo de la restauración. Además de servir como palanca y motivación para que cada vez sean más los establecimientos que formen parte de este proyecto.</p>
            <h2 className="HomeAbout__subtitle">¿Cómo lo llevaremos a cabo?</h2>
            <div className="HomeAbout__card-container">
                <div className="HomeAbout__card">
                    <img src={logoAdmin} alt="Icono de gestion" />
                    <p>Introduciendo criterios de sostenibilidad en la gestión del restaurante</p>
                </div>
                <div className="HomeAbout__card">
                <img src={logoDesign} alt="Icono de diseño de estrategia" />
                    <p>Diseñando la mejor estrategia adaptada tus necesidades</p>
                </div>
                <div className="HomeAbout__card">
                <img src={logoFoot} alt="Icono de huella ambiental" />
                    <p>Reduciendo la huella ambiental y contribuir a que nuestra sociedad sea más justa y sostenible</p>
                </div>
            </div>
        </section>
    )
}

export default HomeAbout;