import { Link } from "react-router-dom";
import { scrollToTop } from '../../utils/scrollToTop';
import "./styles.scss";

function SmallArticle({ artData }) {

  return (
    <section className="SmallArticle">
      {artData
        ? artData.map((article, id) => {
            return (
              <article className="SmallArticle__article" key={article.id} id={id} onClick={scrollToTop}>
                <Link to={`/blog-post/${article.id}`}>
                  <div className="SmallArticle__img">
                    <img src={article.hero} alt={article.title} />
                  </div>

                  <div className="SmallArticle__info">
                    <p>{article.time}</p>
                    <h3>{article.title}</h3>
                  </div>
                </Link>
              </article>
            );
          })
        : null}
    </section>
  );
}
export default SmallArticle;
