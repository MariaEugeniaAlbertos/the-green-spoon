import { Route, Switch } from 'react-router-dom';
import { useState, useEffect } from 'react';

import data from "./components/data/data.json";
import Header from './components/Header';
import Footer from './components/Footer';

import Blog from './pages/blog';
import Home from './pages/home';
import Contact from './pages/contact';
import BlogPost from './pages/blogPost';

import useTextInput from './hooks/useTextInput';
import { filterArticles } from './utils/filter.articles';

import './App.css';


function App() {
  const [artData, setArtData] = useState([]);
  useEffect(() => {
    setArtData([...data])
  }, []);
  
  
  const [filter, changeFilter] = useTextInput('');
  
  const filteredArticles = filterArticles(artData, filter);
 
  return (
    <div className="App">
    <Header/>
      <Switch>
        <Route path="/" exact>
          <Home artData={artData}/>
        </Route>

        <Route path="/blog" exact>
          <Blog artData={filteredArticles} changeFilter={changeFilter}  filter={filter}/>
        </Route>

        <Route path="/blog-post/:id" exact>
          <BlogPost artData={artData}/>
        </Route>
        <Route path="/contact" exact>
          <Contact/>
        </Route>

      </Switch>
      <Footer/>
    </div>
  );
}

export default App;
