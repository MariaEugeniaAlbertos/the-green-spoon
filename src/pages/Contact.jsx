import "../styles/Contact.scss";

import ContactForm from "../components/ContactForm/index";

function Contact() {
  
  return (
    <section className="Contact">
      <article className="Contact__hero"></article>
      <article className="Contact__form-container">
        <div className="Contact__info">
          <h1>¿Quieres ponerte en contacto con nosotras? <br/> ¡Escríbenos!</h1>
          <p>Si quieres colaborar con nosotras, hacernos una consulta o proponernos una entrevista, envíanos un mensaje a través de nuestro formulario o escríbenos a nuestro correo thegreenspoon2021@gmail.com. Te contestaremos lo antes posible.</p>
        </div>
        <ContactForm />
      </article>
    </section>
  );
}

export default Contact;
