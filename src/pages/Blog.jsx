import SmallArticle from "../components/SmallArticle";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import "../styles/Blog.scss";

function Blog({ artData, changeFilter, filter }) {
  return (
    <section className="Blog">
      <div className="Blog__hero-img"></div>
      <div className="Blog__filter-container">
      <h4>Encuentra un artículo</h4>
          <div className="Blog__filter">
            <input type="text" value={filter} onChange={changeFilter} />
            <FontAwesomeIcon icon={faSearch} className="Blog__filter-icon"/>
          </div>
        </div>
      <article className="Blog__desc">
        <p>THE GREEN SPOON</p>
        <h2>Descubre nuestro artículos</h2>
      </article>

      <article className="Blog__articles">
        <SmallArticle artData={artData} />
      </article>
    </section>
  );
}

export default Blog;
