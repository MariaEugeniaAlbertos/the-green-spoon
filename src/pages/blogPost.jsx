import { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faHeart } from "@fortawesome/free-solid-svg-icons";
import { faHeart as faHeartRegular } from "@fortawesome/free-regular-svg-icons";
import SmallArticle from "../components/SmallArticle";

import "../styles/BlogPost.scss";

function BlogPost({ artData }) {
  const [recommendArticle, setRecommendArticle] = useState([]);
  const params = useParams();
  const { id } = params;
  const article = artData.find((article) => {
    return article.id.toString() === id;
  });


  useEffect(() => {
    const artDataFiltered = artData.filter((article) => {
      return article.id < 4;
    });
    setRecommendArticle([...artDataFiltered]);
  }, [artData]);

  const history = useHistory();

  const [isActive, setIsActive] = useState(false);
  const [favorite, setFavorite] = useState("");
  const [like, setLike] = useState(0);
  



  function addLike(id) {
    const newCount = like + 1;
    setLike(newCount)
    setFavorite([...favorite, id]);

    

  }

  return (
    <section className="BlogPost">
      <article>
        {/* Articulo desarrollado */}

        {article ? (
          <article className="BlogPost__article">
            <div className="BlogPost__article-hero">
              <img src={article.hero} alt={article.title} />
            </div>

            <div className="BlogPost__article-txt">
                <h2>{article.title}</h2>
              </div>
              <div className="BlogPost__spans">
                <span>{article.date}</span>
                <div></div>
                <span>{article.time}</span>
              </div>
            <p className="BlogPost__desc">{article.description}</p>

            <button
              id={article.id}
              className="BlogPost__heart"
              onClick={() => {
                addLike(article.id);
                setIsActive(!isActive);
                
              }}
              disabled={isActive}
            >
              <FontAwesomeIcon icon={isActive ? faHeart : faHeartRegular} />{" "}
              {like}
            </button>
            <button onClick={history.goBack} className="BlogPost__goBack">
              <FontAwesomeIcon icon={faArrowLeft} className="BlogPost__goBack-icon"/> Volver atrás
            </button>
          </article>
        ) : null}
      </article>
      <h2 className="BlogPost__h2">Artículos Recomendados</h2>
      <SmallArticle artData={recommendArticle} />
    </section>
  );
}
export default BlogPost;
