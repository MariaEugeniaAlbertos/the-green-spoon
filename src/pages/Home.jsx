import HomeAbout from "../components/HomeAbout";
import HomeArticles from "../components/HomeArticles";
import { Link } from "react-router-dom";

import { scrollToTop } from '../utils/scrollToTop';

import "../styles/Home.scss";

function Home({ artData }) {
  // const filteredArticles = filterArticles(artData, filter);
  return (
    <section className="Home">
      <article className="Home__hero">
        <h1 className="Home__title">
          “Aprende a gestionar tu restaurante de una forma sostenible y
          eficiente”
        </h1>
        <Link to="/contact">
          <button className="Home__btn" type="button" onClick={scrollToTop}>Contacta con nosotros</button>
        </Link>
      </article>

      <HomeAbout />
      <article className="Home__articles">
        <h2 className="Home__articles-subtitle">Articulos destacados</h2>
        <HomeArticles artData={artData} />
      </article>
    </section>
  );
}

export default Home;
