export const cleanString = (str) => str?.trim().toLowerCase();

// filter es lo que el usuario escribe en el input
export const filterArticles = (artData, filter) => {
  const cleanFilter = cleanString(filter);

  const filteredArticles = artData.filter((article) => {
    return (
        cleanString(article.title).includes(cleanFilter) ||
        cleanString(article.description).includes(cleanFilter) ||
        cleanString(article.copy).includes(cleanFilter) ||
        cleanString(article.date).includes(cleanFilter)
    );
  });
  return filteredArticles;
};
